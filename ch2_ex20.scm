
; Examples.

; (same-parity 1 2 3 4 5 6 7)
; (1 3 5 7)

; (same-parity 2 3 4 5 6 7)
; (2 4 6)

(define (same-parity . items)
  (define target-parity (remainder (car items) 2))
  (define (same-parity-local elems)
    (cond ((null? elems) '())
          ((= (remainder (car elems) 2) target-parity)
           (cons (car elems) (same-parity-local (cdr elems))))
          (else (same-parity-local (cdr elems)))))
  (same-parity-local items))

