(define (count-pairs x)
  (if (not (pair? x))
    0
    (+ (count-pairs (car x))
       (count-pairs (cdr x))
       1)))

(newline)

(define x (cons 'a (cons 'b (cons 'c '()))))
(display (count-pairs x))  ; returns 3
(newline)

(define temp (cons 'b '()))
(define y (cons (cons 'a temp) temp))
(display (count-pairs y))  ; returns 4
(newline)

(define temp-1 (cons 'a 'b))
(define temp-2 (cons temp-1 temp-1))
(define z (cons temp-2 temp-2))  ; returns 7
(display (count-pairs z))
(newline)

(define t (cons 'a (cons 'b (cons 'c '()))))
(set-cdr! (cddr t) t)
; (display (count-pairs t))  ; never returns -- cycles forever
; (newline)
