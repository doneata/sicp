
(define (equal? list-1 list-2)
  (cond ((and (null? list-1) (null? list-2)) true)
        ((and (null? list-1) (not (null? list-2))) false)
        ((and (not (null? list-1)) (null? list-2)) false)
        ((and (symbol? list-1) (symbol? list-1)) (eq? list-1 list-2))
        ((and (list? list-1) (list? list-2))
         (and (equal? (car list-1) (car list-2))
              (equal? (cdr list-1) (cdr list-2))))
        (else false)))

