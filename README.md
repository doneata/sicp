
My take on the [Structure and Interpretation of Computer Programs](https://mitpress.mit.edu/sicp/full-text/book/book.html) (SICP) book.

Notes on the exercises from SICP
--------------------------------

**Exercise 2.4.** This exercise suggests an alternative way of representing pairs: we redefine the procedures `cons`, `car` and `cdr` using only functions. To check that indeed `(car (cons x y))` is `x`, we use the substitution model:

```
#!scheme
(car (cons x y))
(car (lambda (m) (m x y)))
((lambda (m) (m x y)) (lambda (p q) p))
((lambda (p q) p) x y)
x
```

**Exercise 2.5.** Church numerals. We are using the substitution model to find the value of `one` and `two`.

```
#!scheme
(add-1 zero)
(add-1 (lambda (f) (lambda (x) x)))
(lambda (f) (lambda (x) (f (((lambda (f) (lambda (x) x)) f) x))))
(lambda (f) (lambda (x) (f x)))
```

**Exercise 2.9.** In this exercise, we are showing that the width of the sum (or difference) of two intervals is a function only of the widths of the intervals being added (or subtracted). Let us denote by `x` the interval `[a, b]` and by `y` the interval `[c, d]`. The addition `x + y` of the two intervals will be an interval `[a + c, b + d]`; this interval has a width of `0.5 * (b - a + d - c)` which is the sum of widths of the two intervals `x` and `y`. The substraction `x - y` will be the interval `[a - d, b - c]`; this interval has a width of `0.5 * (b - a + d - c)` which again is the sum of the widths of the two intervals `x` and `y`.

**Exercise 2.11.**

|                   | A | A | A |   |   | D | B |   |   | B |   | C | C |   |   | A |
|-------------------|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
| `(upper-bound x)` | + | + | + | + | - | + | + | - | + | - | - | + | - | - | - | - |
| `(upper-bound y)` | + | + | + | - | + | + | - | + | - | + | - | - | + | - | - | - |
| `(lower-bound x)` | + | + | - | + | + | - | + | + | - | - | + | - | - | + | - | - |
| `(lower-bound y)` | + | - | + | + | + | - | - | - | + | + | + | - | - | - | + | - |

In total there are 16 cases. But seven of those are invalid, because the upper bound is negative, while the lower bound is positive. This leaves us with nine valid cases, which can be classified into

```
#!scheme

```

**Exercise 2.22.** The first implementation of `square-list` returns the reversed list because each time we call the inner function `iter` we insert an element at the beginning of the `answer` variable. The `answer` modifies with each iteration as follows:

```
#!scheme
(cons e1 '())
(cons e2 '(e1))
(cons e3 '(e2 e1))
```

In the second implementation of `square-list` the elements will be in the right order, but the output answer won't be a list, but a series of nested lists:

```
#!scheme
(((() . e1) . e2) . e3)
```

**Exercise 2.24.** Evaluating the given expression gives:

```
#!scheme
(list 1 (list 2 (list 3 4)))
(1 (2 (3 4)))
```

**Exercise 2.25.** Combinations of `car`s and `cdr`s that will pick the value `7` from each of the lists:

```
#!scheme
(car (cdr (car (cdr (cdr '(1 3 (5 7) 9))))))
(car (car '((7))))
(car (cdr (car (cdr (car (cdr (car (cdr (car (cdr (car (cdr '(1 (2 (3 (4 (5 (6 7))))))))))))))))))
```
**Exercise 2.26.** Here is what's printed by the interpreter when evaluating each of the following expressions:

```
#!scheme
(define x (list 1 2 3))
(define y (list 4 5 6))

(append x y)
(1 2 3 4 5 6)

(cons x y)
((1 2 3) 4 5 6)

(list x y)
((1 2 3) (4 5 6))
```

**Exercise 2.38.** The reduce functions `fold-right` and `fold-left`. We start off by evaluating the given examples:

```
#!scheme
(fold-right / 1 (list 1 2 3))
3/2
(fold-left / 1 (list 1 2 3))
1/6
(fold-right list '() (list 1 2 3))
(1 (2 (3 ())))
(fold-left list '() (list 1 2 3))
(((() 1) 2) 3)
```

We notice that the two reducing functions result in different outputs for the same input arguments. To understand the behaviour of the functions we employ the substitution model: the `fold-right` function expands to `(op e1 (op e2...(op en init)...))`, while the `fold-left` function expands to `(op (op...(op init e1)...en-1) en)`.  It can be showed through induction that the property that the function `op` has to satisfy to guarantee the same results is [associativity](http://en.wikipedia.org/wiki/Associative_property): *f(f(x, y), z) = f(x, f(y, z))*.

**Exercise 2.55.**
The expression `(car ''abracadabra)` is expanded to `(car (quote (quote abracadabra)))`.
So we have a list with two symbols `quote` and `abracadabra`
and taking its `car` returns the first element, that is, `quote`.

**Exercise 2.60.**
If we allow duplicates in the set representation we only need to modify the `adjoin-set` function.
We do not have to check if an element already exists in the list before `cons`ing it to the list.
This reduces the numbers of steps required by `adjoin-set` from `O(n)` to `O(1)`.
The function `union-set` depends on `adjoin-set`, so its complexity is also be reduced: from `O(n^2)` to `O(n)`.
The cost of the other two operations `element-of-set` and `intersection-set` are unchanged.
The disadvantages of this representation becomes evident when we have to store many duplicates.
This case does not affect only the storage requirements, but also the computational cost (as `n` increases the number of operations increase).
The representation of sets as list with duplicates can be useful when the most common operations are adding an element to the set or computing the union of two sets.

**Exercise 2.63.**
(a) The two implementations are identical in terms of results,
but `tree->list-1` is a recursive process,
while `tree->list-2` is an iterative process
(see [Section 1.2.1](https://mitpress.mit.edu/sicp/full-text/book/book-Z-H-11.html#%_sec_1.2.1)
for a discussion on the differences between a recursive and an iterative process).
The list obtained by running either of the functions on any of the trees depicted in Figure 2.16 is `(1 3 5 7 9 11)`.
(b) We assume a balanced tree with `n` elements and
an implementation of the `append` function that scales in the length of the first argument (such as the one described at the end of [Section 2.2.1](https://mitpress.mit.edu/sicp/full-text/book/book-Z-H-15.html#%_sec_2.2.1)).
The function `tree->list-1` has a order of growth of `O(n.log(n))`.
We first note that the cost of merging the lists of two subtrees of size `m/2` is `O(m/2)`.
Then because of the recursive nature of the process the total number of operations is
`n / 2 + 2 * n / 4 + ... + n / 2 * 1 = log(n) n^2 / 2`, which is bounded by `O(n.log(n))`.
The function `tree->list-2` has a linear order of growth, `O(n)`.
Each `entry` element is `cons`ed onto the `result-list` and
as there are `n` elements and the cost of `cons` is `O(1)`,
the total number of operations is `O(n)`.

**Exercise 2.64.**
(a) The function `partial-tree` builds a tree with
the entry as the `n / 2` element of the `elts` list,
the left child as the `partial-tree` built using the elements from `1` to `n / 2 - 1`,
the right child as the `partial-tree` built using the elements from `n / 2 + 1` to `n`.
The second element of the returned pair is the list of the unused elements from `elts`,
that is, the elements from `n` until the end of that list.
(b) The order of growth of the `list->tree` function is `O(n)`,
because there is no overlapping computation done in the recursive steps,
and the cost of each step is `O(1)`, the `cons` operation.

**Exercise 2.70.**
It takes 84 bits to encode the song using Huffman encoding.
It would have taken 108 bits if we were to use a fixed-length code
(3 bits for each word of the alphabet and there are 34 words in the song).

**Exercise 2.71.**
The Huffman coding for a list of frequencies of 1, 2,...,2^(n-1) would look as follows:
at the `i` step it would combine the symbol corresponding to the joint symbol
1, 2,...,2^(i-1) (whose total frequency is 2^i - 1) with the symbol with frequency 2^i.
This results in an unbalanced tree.

**Exercise 3.9.**
For the recursive definition of `factorial`, a frame is created with each recursive call of the `factorial` procedure.
The frame binds the parameter of the procedure, `n`, to the argument of the call
and sets the enclosing environment, `outer-env`, to be the global environment, `global-env`:

```
#!scheme
(factorial 6) ; Creates environment E1 = {n: 6, outer-env: global-env}
(factorial 5) ; Creates environment E2 = {n: 5, outer-env: global-env}
(factorial 4) ; Creates environment E3 = {n: 4, outer-env: global-env}
(factorial 3) ; Creates environment E4 = {n: 3, outer-env: global-env}
(factorial 2) ; Creates environment E5 = {n: 2, outer-env: global-env}
(factorial 1) ; Creates environment E6 = {n: 1, outer-env: global-env}
(factorial 0) ; Creates environment E7 = {n: 0, outer-env: global-env}
```

For the iterative definition of `factorial`, a frame is created for the initial call of the `factorial` procedure and then a frame is created with each call of the `fact-iter` procedure.
As before, the parameters of the procedure are bound to the arguments of the call
and the enclosing environment, `outer-env`, is the global environment, `global-env`:

```
#!scheme
(factorial 6)       ; Creates environment E1 = {n: 6, outer-env: global-env}
(fact-iter   1 1 6) ; Creates environment E2 = {product:   1, count: 1, max-count: 6, outer-env: global-env}
(fact-iter   1 2 6) ; Creates environment E3 = {product:   1, count: 2, max-count: 6, outer-env: global-env}
(fact-iter   2 3 6) ; Creates environment E4 = {product:   2, count: 3, max-count: 6, outer-env: global-env}
(fact-iter   6 4 6) ; Creates environment E5 = {product:   6, count: 4, max-count: 6, outer-env: global-env}
(fact-iter  24 5 6) ; Creates environment E6 = {product:  24, count: 5, max-count: 6, outer-env: global-env}
(fact-iter 120 6 6) ; Creates environment E7 = {product: 120, count: 6, max-count: 6, outer-env: global-env}
(fact-iter 720 7 6) ; Creates environment E8 = {product: 720, count: 7, max-count: 6, outer-env: global-env}
```

**Exercise 3.10.**
If we remove the syntactic sugar by replacing the `let` expression with the `lambda` expression,
we obtain the following procedure definition of `make-withdraw`:

```
#!scheme
(define (make-withdraw initial-amount)
  ((lambda (balance)
     (lambda (amount)
       (if (>= balance amount)
         (begin (set! balance (- balance amount))
                balance)
         "Insufficient funds")))
   initial-amount)) 
```

Evaluating the definitions of `make-withdraw` and `W1` results in the following environments:

![Environments for exercise 3.10, part 1](ch3_ex10a.png)

Evaluating `(W1 50)` creates a new frame, `E3`, which binds the `amount` to 50 and then updates the `balance`.
The resulting environments are shown below:

![Environments for exercise 3.10, part 2](ch3_ex10b.png)

Finally, evaluating the definition of `W2` will create new frames that bind the variables `initial-amount` and `balance`.

**Exercise 3.12.**
The `append` function doesn't mutate its inputs, so we can apply the substitution rule:
`(cdr x)` is equivalent to `(cdr (list 'a 'b'))`, which is `(b)`.
Instead the `append!` function mutates its inputs:
it changes the first list by appending the second list onto it.
As `x` is the first argument,
`x` will be the appended list,
hence `(cdr x)` will be `(b c d)`.

**Exercise 3.13.**
The procedure `make-cycle` changed the last pair `z` to point to the beginning of `z`,
so `(cdr x)` is never `null` for any pair `x` from `z`.
Hence `(last-pair z)` will run indefinitely. 

**Exercise 3.14.**
The procedure `mystery` reverses a list.

**Exercise 3.21.**
The funny printing is happening because the interpreter shows the auxiliary data structure that maintains the two pointers:
the front pointer and rear pointer.
Printing the contents of the front pointers shows the contents of the queue:

```
#!scheme
(define (print-queue queue) (display (front-ptr queue)) (newline))
```

**Exercise 3.25.**
One could simplify the implementation in two ways:

- Abstract the iterations for the two functions, `lookup` and `insert!`, using the `fold` function.
- Use the original implementation (from the book or exercise 3.24) and store the keys as lists.
The main difference from the current implementation is that the data will be stored in a one-dimensional table.

**Exercise 3.26.**
We can use a [binary search tree](https://en.wikipedia.org/wiki/Binary_search_tree) in order to speed-up the implementation.
Instead of using a list as the back-bone of our data structure, we will use a tree.
Each node in the tree stores a key, a value and pointers to two sub-trees:

- the left sub-tree contains nodes whose values are smaller than the value of the current node.
- the right sub-tree contains nodes whose values are larger than the value of the current node.

We will have to change the `assoc` function to perform binary search.
Using binary search for this operation will reduce the time complexity from `O(n)` to `O(log n)`.

**Exercise 3.27.**
The memoized version,`memo-fib`, will never re-compute previously computed results, but it will retrieve them from the local table.
Hence, the call tree is a linear list and in order to compute the `n`-th term it will need `n` steps.
If we define `memo-fib` to be `(memoize fib)`, the recursive calls will still call the original version, `fib`;
so this scheme won't bring any improvement.

**Exercise 3.31.**
Whenever a wire's signal value is changed, its corresponding action procedures are added to the agenda (waiting to be executed).
However, initially the wires have a default value (zero) and the agenda is empty.
So, at initialization we need to populate the agenda in a different manner:
by running each action procedure when we create a new wire.

**Exercise 3.32.**
Using a first in, first out (FIFO) execution model makes sense because we would like the output signal to follow the order of updates in the input.
Using a last in, first out (LIFO) execution model will change the output in the reversed order of the input modifications, yielding an erroneous update.

**Exercise 3.34.**
The problem with having `squarer` defined as `(multiplier a a b)` is that the propagation of constraints doesn't work in both directions: we can compute the `b` given `a`, but we cannot compute `a` given `b`.
This limitation happens because `multiplier` requires two of its three connectors to have a value and in the `squarer` case only one connector has a value, `b`, while the other two are unspecified.
