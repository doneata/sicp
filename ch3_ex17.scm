(define (contains? xs x)
  (cond ((null? xs) #f)
        ((eq? (car xs) x) #t)
        (else (contains? (cdr xs) x))))

(define count-pairs
  (let ((xs '()))
    (lambda (x)
      (if (or (not (pair? x)) (contains? xs x))
        0
        (begin
          (set! xs (cons x xs)) 
          (+ (count-pairs (car x))
             (count-pairs (cdr x))
             1))))))

(newline)

(define x (cons 'a (cons 'b (cons 'c '()))))
(display (count-pairs x))  ; returns 3
(newline)

(define temp (cons 'b '()))
(define y (cons (cons 'a temp) temp))
(display (count-pairs y))  ; returns 3
(newline)

(define temp-1 (cons 'a 'b))
(define temp-2 (cons temp-1 temp-1))
(define z (cons temp-2 temp-2))  ; returns 3
(display (count-pairs z))
(newline)

(define t (cons 'a (cons 'b (cons 'c '()))))
(set-cdr! (cddr t) t)
(display (count-pairs t))  ; returns 3
(newline)
