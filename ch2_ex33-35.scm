
; Utils
(define (accumulate op initial sequence)
  (if (null? sequence)
    initial
    (op (car sequence)
        (accumulate op initial (cdr sequence)))))

; Exercise 2.33
(define (my-map p sequence)
  (accumulate (lambda (x y) (cons (p x) y)) '() sequence))

(define (my-append seq1 seq2)
  (accumulate cons seq2 seq1))

(define (my-length sequence)
  (accumulate (lambda (_ y) (+ 1 y)) 0 sequence))

; Exercise 2.34
(define (horner-eval x sequence)
  (accumulate
    (lambda (this-coeff higher-terms) (+ this-coeff (* x higher-terms)))
    0
    sequence))

; Exercise 2.35
(define (count-leaves tree)
  (accumulate
    (lambda (x y) (+ (length x) y))
    0
    (map fringe tree)))

