(define (append xs ys)
  (if (null? xs)
    ys
    (cons (car xs) (append (cdr xs) ys))))

(define (append! xs ys)
  (set-cdr! (last-pair xs) ys)
  xs)

(define (last-pair xs)
  (if (null? (cdr xs))
    xs
    (last-pair (cdr xs))))

(define x (list 'a 'b))
(define y (list 'c 'd))
(define z (append x y))
(display (cdr x)) ; (b)
(define w (append! x y))
(display (cdr x)) ; (b c d)
