
(define (last-pair sequence)
  (if (= (length sequence) 1) (car sequence)
    (last-pair (cdr sequence))))

