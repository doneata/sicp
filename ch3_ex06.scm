(define rand-update
  (let ((a 1664525)
        (c 1013904223)
        (m (expt 2 32)))
    (lambda (x) (modulo (+ (* a x) c) m))))

(define rand
  (let* ((INIT-STATE 42)
         (state INIT-STATE))
    (lambda (todo)
      (cond ((eq? todo 'generate)
             (begin
               (set! state (rand-update state))
               state))
            ((eq? todo 'reset)
             (lambda (new-state)
               (set! state new-state)
               (display "Random state reset")))
            (else (error "Unknown option"))))))
