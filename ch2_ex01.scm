
; Helper functions.
(define (gcd a b)
  (if (= b 0)
    a
    (gcd b (remainder a b))))

; Constructor
(define (make-rat n d)
  (let ((g (gcd (abs n) (abs d)))
        (sgn (if (< d 0) -1 +1)))
    (cons (* sgn (/ n g))
          (* sgn (/ d g)))))

; Selectors
(define numer car)
(define denom cdr)

; Printing function.
(define (print-rat x)
  (newline)
  (display (numer x))
  (display "/")
  (display (denom x)))

; Examples.
(print-rat (make-rat +12 +15))
(print-rat (make-rat +12 -15))
(print-rat (make-rat -12 +15))
(print-rat (make-rat -12 -15))

