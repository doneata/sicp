
; Derivation rules.
(define (deriv exp var)
  (cond ((number? exp) 0)
        ((variable? exp)
         (if (same-variable? exp var) 1 0))
        ((sum? exp)
         (make-sum (deriv (addend exp) var)
                   (deriv (augend exp) var)))
        ((product? exp)
         (make-sum
           (make-product (multiplier exp)
                         (deriv (multiplicand exp) var))
           (make-product (deriv (multiplier exp) var)
                         (multiplicand exp))))
        (else (error "unknown expression type -- DERIV" exp))))

(define (variable? x) (symbol? x))
(define (same-variable? v1 v2) (and (variable? v1) (variable? v2) (eq? v1 v2)))
(define (same-symbol? v1 v2) (and (symbol? v1) (symbol? v2) (eq? v1 v2)))

(define (=number? exp num) (and (number? exp) (= exp num)))
(define (take-until f xs) (if (f (car xs)) '() (cons (car xs) (take-until f (cdr xs)))))
(define (drop-until f xs) (if (f (car xs)) (cdr xs) (drop-until f (cdr xs))))
(define (in-list x xs) (any (lambda (e) (same-symbol? e x)) xs))
(define (unpack-one x) (if (= (length x) 1) (car x) x))

; Sum
(define (make-sum a1 a2)
  (cond ((=number? a1 0) a2)
        ((=number? a2 0) a1)
        ((and (number? a1) (number? a2)) (+ a1 a2))
        (else (list a1 '+ a2))))

(define (sum? x) (and (pair? x) (in-list '+ x)))

(define (addend s) (unpack-one (take-until (lambda (x) (same-symbol? x '+)) s)))
(define (augend s) (unpack-one (drop-until (lambda (x) (same-symbol? x '+)) s)))

; Product
(define (make-product m1 m2)
  (cond ((or (=number? m1 0) (=number? m2 0)) 0)
        ((=number? m1 1) m2)
        ((=number? m2 1) m1)
        ((and (number? m1) (number? m2)) (* m1 m2))
        (else (list m1 '* m2))))

; It's not necessary to check that '+ is not in the list, because in the
; `deriv` function we first check for `sum` and then for `product`.
(define (product? x) (and (pair? x) (in-list '* x)))

(define (multiplier p) (unpack-one (take-until (lambda (x) (same-symbol? x '*)) p)))
(define (multiplicand p) (unpack-one (drop-until (lambda (x) (same-symbol? x '*)) p)))

