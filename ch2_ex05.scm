
(define (cons x y)
  (* (expt 2 x)
     (expt 3 y)))

(define (count-powers z n)
  (if (> (remainder z n) 0)
    0
    (1+ (count-powers (/ z n) n))))

(define (car z)
  (count-powers z 2))

(define (cdr z)
  (count-powers z 3))

; Example.
(define a (cons 10 5))
(car a)
(cdr a)

