
(define (my-reverse sequence)
  (define (my-reverse-iter sequence reversed-sequence)
    (if (null? sequence) reversed-sequence
      (my-reverse-iter (cdr sequence) (cons (car sequence) reversed-sequence))))
  (my-reverse-iter sequence '()))

