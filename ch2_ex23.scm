
; (my-for-each (lambda (x) (newline) (display x))
;              (list 57 321 88))
; 57
; 321
; 88

(define (my-for-each f items)
  (if (null? items)
    #t
    (begin (f (car items))
           (my-for-each f (cdr items)))))

