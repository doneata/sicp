
(load "ch2_ex2-3.scm")

; Rectangle implementation -- it assumes axis-aligned rectangles.
(define make-rectangle cons)
(define top-left car)
(define bottom-right cdr)

; Alternative implementation.
; (define (make-rectangle top-left width height)
;   (cons top-left (cons width height)))
; (define top-left car)
; (define width cadr)
; (define height cddr)

(define (width r)
  (- (x-point (bottom-right r))
     (x-point (top-left r))))

(define (height r)
  (- (y-point (top-left r))
     (y-point (bottom-right r))))

(define (perimeter r)
  (* (+ (height r) (width r)) 2))

(define (area r)
  (* (height r) (width r)))

; Examples.
(define A (make-point -1 8))
(define B (make-point 0 2))
(define rect (make-rectangle A B))

(perimeter rect)
(area rect)

