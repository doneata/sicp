
; Point implementation.
(define make-point cons)
(define x-point car)
(define y-point cdr)

; Segment implementation.
(define make-segment cons)
(define start-segment car)
(define end-segment cdr)

(define (midpoint-segment s)
  (let ((average (lambda (x y) (/ (+ x y) 2)))
        (p1 (start-segment s))
        (p2 (end-segment s)))
    (make-point (average (x-point p1) (x-point p2))
                (average (y-point p1) (y-point p2)))))

; Pretty printing.
(define (print-point p)
  (newline)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")"))

; Examples.
(define A (make-point 1 2))
(define B (make-point 0 8))
(define AB (make-segment A B))
(define C (midpoint-segment AB))

(print-point A)
(print-point B)
(print-point C)

