(define f
  (let ((i 0))
    (lambda (x)
      (set! i (+ i 1))
      (if (and (= x 1) (= i 1)) 1 0))))

; (+ (f 0) (f 1)) => 0
; (+ (f 1) (f 0)) => 1
