#lang planet neil/sicp

;; Exercise 3.2.
(define (make-monitored f)
  (define count 0)
  (define (mf arg)
    (cond ((eq? arg 'reset) (set! count 0))
          ((eq? arg 'how-many-calls?) count)
          (else (begin (set! count (+ count 1))
                       (f arg)))))
  mf)
