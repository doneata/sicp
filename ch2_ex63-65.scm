
(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))

(define (make-tree entry left right) (list entry left right))
(define (make-leaf entry) (make-tree entry '() '()))

(define (element-of-set? x set)
  (cond ((null? set) false)
        ((= x (entry set)) true)
        ((< x (entry set))
         (element-of-set? x (left-branch set)))
        ((> x (entry set))
         (element-of-set? x (right-branch set)))))

(define (adjoin-set x set)
  (cond ((null? set) (make-tree x '() '()))
        ((= x (entry set)) set)
        ((< x (entry set))
         (make-tree (entry set) 
                    (adjoin-set x (left-branch set))
                    (right-branch set)))
        ((> x (entry set))
         (make-tree (entry set)
                    (left-branch set)
                    (adjoin-set x (right-branch set))))))

(define (tree->list-1 tree)
  (if (null? tree)
    '()
    (append (tree->list-1 (left-branch tree))
            (cons (entry tree)
                  (tree->list-1 (right-branch tree))))))

(define (tree->list-2 tree)
  (define (copy-to-list tree result-list)
    (if (null? tree)
      result-list
      (copy-to-list (left-branch tree)
                    (cons (entry tree)
                          (copy-to-list (right-branch tree)
                                        result-list)))))
  (copy-to-list tree '()))

(define (list->tree elements)
  (car (partial-tree elements (length elements))))

(define (partial-tree elts n)
  (if (= n 0)
    (cons '() elts)
    (let ((left-size (quotient (- n 1) 2)))
      (let ((left-result (partial-tree elts left-size)))
        (let ((left-tree (car left-result))
              (non-left-elts (cdr left-result))
              (right-size (- n (+ left-size 1))))
          (let ((this-entry (car non-left-elts))
                (right-result (partial-tree (cdr non-left-elts) right-size)))
            (let ((right-tree (car right-result))
                  (remaining-elts (cdr right-result)))
              (cons (make-tree this-entry left-tree right-tree)
                    remaining-elts))))))))

(define (intersection-set set1 set2)
  (define (inter set1 set2)
    (if (or (null? set1) (null? set2))
      '()    
      (let ((x1 (car set1))
            (x2 (car set2)))
        (cond ((= x1 x2) (cons x1 (inter (cdr set1) (cdr set2))))
              ((< x1 x2) (inter (cdr set1) set2))
              ((> x1 x2) (inter set1 (cdr set2)))))))
  (list->tree (inter (tree->list-2 set1) (tree->list-2 set2))))

(define (union-set set1 set2)
  (define (union set1 set2)
    (cond ((null? set1) set2)
          ((null? set2) set1)
          (else
            (let ((x1 (car set1))
                  (x2 (car set2)))
              (cond ((= x1 x2) (cons x1 (union (cdr set1) (cdr set2))))
                    ((< x1 x2) (cons x1 (union (cdr set1) set2)))
                    ((> x1 x2) (cons x2 (union set1 (cdr set2)))))))))
  (list->tree (union (tree->list-2 set1) (tree->list-2 set2)))) 

; Pretty printing trees.
(define (print-tree tree level)
  (cond ((null? tree) 'done)
        (else (newline)
              (space-to level)
              (display (entry tree))
              (print-tree (left-branch tree) (+ level 1))
              (print-tree (right-branch tree) (+ level 1)))))

(define (space-to n)
  (if (not (= n 0))
    (begin (display " ")
           (space-to (- n 1)))))

; Define the trees shown in Figure 2.16.
(define x
  (make-tree
    7
    (make-tree 3 (make-leaf 1) (make-leaf 5))
    (make-tree 9 '() (make-leaf 11))))
(define y
  (make-tree
    3
    (make-leaf 1)
    (make-tree
      7
      (make-leaf 5)
      (make-tree 9 '() (make-leaf 11)))))
(define z
  (make-tree
    5
    (make-tree 3 (make-leaf 1) '())
    (make-tree 9 (make-leaf 7) (make-leaf 11))))

; Define the tree shown in Figure 2.17.
(define (make-unbalanced-tree i m)
  (if (= i m)
    (make-leaf m)
    (make-tree i '() (make-unbalanced-tree (+ i 1) m))))
(define t (make-unbalanced-tree 1 7))

