#lang planet neil/sicp

;; Exercise 3.1.
(define (make-accumulator acc)
  (lambda (elem)
    (begin (set! acc (+ acc elem))
           acc)))