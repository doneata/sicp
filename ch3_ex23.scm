; Helper functions

(define (front-ptr dequeue) (car dequeue))

(define (rear-ptr dequeue) (cdr dequeue))

(define (set-front-ptr! dequeue item) (set-car! dequeue item))

(define (set-rear-ptr! dequeue item) (set-cdr! dequeue item))

; Dequeue API

(define (make-dequeue) (cons '() '()))

(define (empty-dequeue? dequeue) (null? (front-ptr dequeue)))

(define (front-dequeue dequeue)
  (if (empty-dequeue? dequeue)
    (error "FRONT-DEQUEUE called with an empty dequeue" dequeue)
    (caar (front-ptr dequeue))))

(define (rear-dequeue dequeue)
  (if (empty-dequeue? dequeue)
    (error "REAR-DEQUEUE called with an empty dequeue" dequeue)
    (caar (rear-ptr dequeue))))

(define (front-insert-dequeue! dequeue item)
  (let ((new-pair (cons (cons item '()) '())))
    (cond ((empty-dequeue? dequeue)
           (set-front-ptr! dequeue new-pair)
           (set-rear-ptr! dequeue new-pair)
           dequeue)
          (else
            (set-cdr! new-pair (front-ptr dequeue)) 
            (set-cdr! (car (front-ptr dequeue)) new-pair)
            (set-front-ptr! dequeue new-pair)
            dequeue)))) 

(define (rear-insert-dequeue! dequeue item)
  (let ((new-pair (cons (cons item '()) '())))
    (cond ((empty-dequeue? dequeue)
           (set-front-ptr! dequeue new-pair)
           (set-rear-ptr! dequeue new-pair)
           dequeue)
          (else
            (set-cdr! (rear-ptr dequeue) new-pair)
            (set-cdr! (car new-pair) (rear-ptr dequeue))
            (set-rear-ptr! dequeue new-pair)
            dequeue))))

(define (front-delete-dequeue! dequeue)
  (cond ((empty-dequeue? dequeue)
         (error "DELETE-DEQUEUE! called with an empty dequeue" dequeue))
        (else
          (set-cdr! (car (front-ptr dequeue)) '())
          (set-front-ptr! dequeue (cdr (front-ptr dequeue)))
          dequeue)))

(define (rear-delete-dequeue! dequeue)
  (cond ((empty-dequeue? dequeue)
         (error "DELETE-DEQUEUE! called with an empty dequeue" dequeue))
        (else
          (set-cdr! (cdar (rear-ptr dequeue)) '())
          (set-rear-ptr! dequeue (cdar (rear-ptr dequeue)))
          dequeue)))

(define (print-dequeue dequeue) 
  (define (print-iter iter)
    (display (caar iter))
    (display " ")
    (if (null? (cdr iter))
      (newline)
      (print-iter (cdr iter))))
  (if (empty-dequeue? dequeue)
    (begin (display "nil") (newline))
    (print-iter (front-ptr dequeue))))

(define d (make-dequeue))
(print-dequeue d)

(define d (front-insert-dequeue! d 'c)) ; c
(define d (front-insert-dequeue! d 'b)) ; b c
(define d (front-insert-dequeue! d 'a)) ; a b c
(define d (rear-insert-dequeue! d 'd))  ; a b c d
(print-dequeue d)

(define d (front-delete-dequeue! d)) ; b c d
(print-dequeue d)

(define d (front-delete-dequeue! d)) ; c d
(print-dequeue d)

(define d (rear-delete-dequeue! d))  ; c
(print-dequeue d)
