#lang racket

(require graphics/graphics)

(open-graphics)

(define *width* 320)
(define *height* 320)

(define viewport (open-viewport "Picture" *width* *height*))

;; Vectors

; Constructer and getters.
; Wraps the `posn` struct, as this required by `draw-line`.
(define make-vect make-posn)
(define xcor-vect posn-x)
(define ycor-vect posn-y)

; Operations.
(define (op-vect op)
  (λ (vect-1 vect-2)
    (make-vect (op (xcor-vect vect-1) (xcor-vect vect-2))
               (op (ycor-vect vect-1) (ycor-vect vect-2)))))

(define add-vect (op-vect +))
(define sub-vect (op-vect -))
(define (scale-vect s v)
  (make-vect (* s (xcor-vect v)) (* s (ycor-vect v))))

;; Frames

; Constructer and getters.
(define (make-frame origin edge1 edge2)
  (list origin edge1 edge2))
(define origin-frame car)
(define edge1-frame cadr)
(define edge2-frame caddr)

; Operations.
(define (frame-coord-map frame)
  (lambda (v)
    (add-vect
     (origin-frame frame)
     (add-vect (scale-vect (xcor-vect v)
                           (edge1-frame frame))
               (scale-vect (ycor-vect v)
                           (edge2-frame frame))))))

;; Segments

; Constructer and getters.
(define make-segment cons)
(define start-segment car)
(define end-segment cdr)

; Operations.
(define (segments->painter segment-list)
  (lambda (frame)
    (for-each
      (lambda (segment)
        ((draw-line viewport)
         ((frame-coord-map frame) (start-segment segment))
         ((frame-coord-map frame) (end-segment segment))))
      segment-list)))

; Example.

(define tl (make-vect 0 0))
(define tr (make-vect 0 *width*))
(define bl (make-vect *height* 0))
(define br (make-vect *height* *width*))

(define frame (make-frame tl (make-vect 0 1) (make-vect 1 0)))

; Exercise 2.49, a.
(define border (list (make-segment tl tr)
                     (make-segment tr br)
                     (make-segment br bl)
                     (make-segment bl tl)))
((segments->painter border) frame)

; Exercise 2.49, b.
(define X (list (make-segment tl br)
                (make-segment tr bl)))
((segments->painter X) frame)

; Exercise 2.49, c.
(define (midpoint u v) (scale-vect 0.5 (add-vect u v)))

(define mt (midpoint tl tr))
(define mr (midpoint tr br))
(define mb (midpoint br bl))
(define ml (midpoint bl tl))

(define diamond (list (make-segment  mt mr)
                      (make-segment mr mb)
                      (make-segment mb ml)
                      (make-segment ml mt)))
((segments->painter diamond) frame)


